<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfluencersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 191);
            $table->string('last_name', 191);
            $table->string('language', 191)->nullable();
            $table->string('title', 191)->nullable();
            $table->text('description')->nullable();
            $table->string('email')->unique();
            $table->string('avatar', 191)->nullable();
            $table->string('gender', 191)->nullable();
            $table->integer('age')->nullable();
            $table->string('country', 191)->nullable();
            $table->string('town', 191)->nullable();
            $table->text('address')->nullable();
            $table->string('code_postal', 191)->nullable();
            $table->string('phone', 191)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('verified')->default(false);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencers');
    }
}
