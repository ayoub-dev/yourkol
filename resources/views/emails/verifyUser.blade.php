<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>

    <h2>Bonjour {{$influencer['first_name']}} {{$influencer['last_name']}},<h2>

    <p>Veuillez activer votre compte en cliquant sur le bouton ci-dessous :</p>

    <br/>
    <a href="{{url('user/verify', $influencer->verifyUser->token)}}">Activer mon compte</a>

    <p><strong>L&rsquo;&eacute;quipe Yourkol&nbsp;</strong><br /><br />PS : Suivez-nous sur&nbsp;
        <a href="#" target="_blank" rel="noopener">
            <span style="text-decoration: underline;">Facebook</span>&nbsp;
        </a>
    </p>
</body>

</html>

