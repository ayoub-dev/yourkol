<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $influencer = Auth::user();
        $reseaux_sociaux = $influencer->platforms;
        $facebook = $influencer->platforms->where('name', 'facebook')->first();
        $instagram = $influencer->platforms->where('name', 'instagram')->first();
        $twitter = $influencer->platforms->where('name', 'twitter')->first();
        $youtube = $influencer->platforms->where('name', 'youtube')->first();
        $website = $influencer->platforms->where('name', 'website')->first();

        //dd($reseaux_sociaux);
        return view('home', compact('influencer', 'reseaux_sociaux', 'facebook', 'instagram', 'twitter', 'youtube', 'website'));
    }
}
