<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Influencer;

class Platform extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'url', 'followers'
    ];

    public function influencer()
    {
        return $this->belongsTo(Influencer::class, 'user_id');
    }
}
