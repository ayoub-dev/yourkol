<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Influencer;

class VerifyUser extends Model
{
    protected $guarded = [];

    public function influencer()
    {
        return $this->belongsTo(Influencer::class, 'user_id');
    }
}
